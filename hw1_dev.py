"""M3C 2017 Homework 1
"""
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

def rw2d(Nt,M,a=0,b=0):
    """Input variables
    Nt: number of time steps
    M: Number of simulations
    a,b: bias parameters
    """

    X = np.zeros((Nt+1,M))
    Y = np.zeros((Nt+1,M))# Preallocate memory for two zero matrices

    for i in range(M):# M times simulations
        for j in range(Nt):# Nt steps in each simulation

            prob1=np.random.rand()
            prob2=np.random.rand()
            if prob1<=0.5:
                P = -1
            else:
                P = 1+a
            if prob2<=0.5:
                Q = -1
            else:
                Q = 1-b


            X[j+1,i] = X[j,i] + P
            Y[j+1,i] = Y[j,i] + Q# the movememt of X and Y

    X_Square = X**2
    Y_Square = Y**2
    XY = np.multiply(X,Y)#construct the values for X^2,Y^2 and XY

    X_Mean = np.mean(X, axis = 1)
    Y_Mean = np.mean(Y, axis = 1)
    X_SquareMean = np.mean(X_Square, axis=1)
    Y_SquareMean = np.mean(Y_Square, axis=1)
    XY_Mean = np.mean(XY, axis=1)

    return X_Mean, Y_Mean, X_SquareMean, Y_SquareMean, XY_Mean



def rwnet1(H,Hf,a=0,display=False):
    """Input variables
    H: Height at which new nodes are initially introduced
    Hf: Final network height
    a: horizontal bias parameter, should be -1, 0, or 1
    display: figure displaying the network is created when true
    Output variables
    X,Y: Final node coordinates
    output: a tuple containing any other information you would
    like the function to return, may be left empty
    """
    X=[0]
    Y=[0]
    d_1=np.sqrt(1+(1+a)**2)

    while True:
        x=0
        y=H
        d=np.sqrt(x**2+y**2)
        while d>d_1:
            px=np.random.rand()
            py=np.random.rand()
            if px<0.5:
                x=x+1+a
            else:
                x=x-1
            if py<0.5:
                y=y
            else:
                y=y-1
            d=min([np.sqrt((x-X[i])**2+(y-Y[i])**2) for i in range(len(X))])
            if y==0:
                break
        if d<=d_1:
            X=X+[x]
            Y=Y+[y]
        if y==Hf:
            break

    if display==True:
        plt.scatter(X,Y)
        plt.show()
    return Y



def rw(H,Hf,b=1,a=0,display=False):

    d_1=np.sqrt(1+(1+a)**2)

    X_network=[0]
    Y_network=[0]

    while True:

        X=0
        Y=H
        d=np.sqrt(X**2+Y**2)

        while Y!=0 and d>d_1:

            prob1=np.random.rand()
            if prob1<=0.5:
                P=-1
            else:
                P=1+a
            X=X+P

            prob2=np.random.rand()
            if prob2<=0.5:
                Q=-1
            else:
                Q=1-b
            Y=Y+Q

            
            for i in range(len(X_network)):
                d=np.sqrt((X-X_network[i])**2+(Y-Y_network[i])**2)
                if d<=d_1 :
                    break
        if d<=d_1:
            X_network.append(X)
            Y_network.append(Y)

        if Y==Hf:
            break
    if display==True:
        plt.scatter(X_network,Y_network)
        plt.show()

    return X_network, Y_network


def rwnet2(L,H,Hf,a=0,display=False):
    """Input variables
    L: Walls are placed at X = +/- L
    H: Height at which new nodes are initially introduced
    Hf: Final network height
    a: horizontal bias parameter, should be -1, 0, or 1
    display: figure displaying the network is created when true
    Output variables
    X,Y: Final node coordinates
    output: a tuple containing any other information you would
    like the function to return, may be left empty
    """

    return X,Y,output

def analyze():
    """ Add input variables as needed
    """

def network(X,Y,dstar,display=False,degree=False):
    """ Input variables
    X,Y: Numpy arrays containing network node coordinates
    dstar2: Links are placed between nodes within a distance, d<=dstar of each other
        and dstar2 = dstar*dstar
    display: Draw graph when true
    degree: Compute, display and return degree distribution for graph when true
    Output variables:
    G: NetworkX graph corresponding to X,Y,dstar
    D: degree distribution, only returned when degree is true
    """



if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    analyze()
